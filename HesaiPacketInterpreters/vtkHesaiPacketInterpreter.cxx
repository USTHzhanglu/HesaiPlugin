/*=========================================================================

  Program:   LidarView
  Module:    vtkHesaiPacketInterpreter.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkHesaiPacketInterpreter.h"
#include "vtkHelper.h"
#include "vtkHesaiSwiftDecoder.h"

#include "InterpreterHelper.h"

#include <vtk_hesai_general_sdk.h>

#include <vtkDoubleArray.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkTransform.h>
#include <vtkTypeUInt64Array.h>
#include <vtkUnsignedIntArray.h>

#include <filesystem>
#include <functional>
#include <vector>

namespace hs_general = hesai::general;

//-----------------------------------------------------------------------------
class vtkHesaiPacketInterpreter::vtkInternals
{
public:
  //-----------------------------------------------------------------------------
  void CreateDecoder(std::function<void(double timestamp)> splitFrameCallback)
  {
    auto generalFillPoint =
      std::bind(&vtkHesaiPacketInterpreter::vtkInternals::FillPoint<hs_general::Point>,
        this,
        std::placeholders::_1);
    this->GeneralDecoder =
      std::make_unique<hs_general::PandarGeneral_Internal>(generalFillPoint, splitFrameCallback);

    auto swiftFillPoint =
      std::bind(&vtkHesaiPacketInterpreter::vtkInternals::FillPoint<vtkHesaiSwiftDecoder::Point>,
        this,
        std::placeholders::_1);
    this->SwiftDecoder = std::make_unique<vtkHesaiSwiftDecoder>(swiftFillPoint, splitFrameCallback);
  }

  //-----------------------------------------------------------------------------
  std::string GetLidarModelName(int model)
  {
    if (model == 128)
    {
      return "Pandar128";
    }
    const std::vector<std::string> supportedList = this->GeneralDecoder->GetSupportedLidarList();
    return supportedList[model];
  }

private:
  //-----------------------------------------------------------------------------
  template <typename PointT>
  void FillPoint(PointT point)
  {
    // When scanning all packets to build an index map do not fill points.
    if (this->IsPreProcessing)
    {
      return;
    }

    double pos[3] = { point.x, point.y, point.z };
    if (std::isnan(point.x) || std::isnan(point.y) || std::isnan(point.z))
    {
      return;
    }

    double distance =
      std::sqrt(std::pow(0 - point.x, 2) + std::pow(0 - point.y, 2) + std::pow(0 - point.z, 2));
    // Skip wrong points
    if (distance <= 0.1 || distance > 500.0)
    {
      return;
    }

    this->Points->InsertNextPoint(pos);
    InsertNextValueIfNotNull(this->PointsX, pos[0]);
    InsertNextValueIfNotNull(this->PointsY, pos[1]);
    InsertNextValueIfNotNull(this->PointsZ, pos[2]);
    InsertNextValueIfNotNull(this->Intensity, point.intensity);
    InsertNextValueIfNotNull(this->LaserId, point.ring);
    InsertNextValueIfNotNull(this->Timestamp, point.timestamp);
    InsertNextValueIfNotNull(this->Distance, distance);
  }

public:
  bool IsPreProcessing = true;

  vtkSmartPointer<vtkPoints> Points;
  vtkSmartPointer<vtkDoubleArray> PointsX;
  vtkSmartPointer<vtkDoubleArray> PointsY;
  vtkSmartPointer<vtkDoubleArray> PointsZ;
  vtkSmartPointer<vtkUnsignedCharArray> Intensity;
  vtkSmartPointer<vtkUnsignedCharArray> LaserId;
  vtkSmartPointer<vtkDoubleArray> Distance;
  vtkSmartPointer<vtkDoubleArray> Timestamp;

  std::unique_ptr<hs_general::PandarGeneral_Internal> GeneralDecoder;
  std::unique_ptr<vtkHesaiSwiftDecoder> SwiftDecoder;
};

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkHesaiPacketInterpreter)

//-----------------------------------------------------------------------------
vtkHesaiPacketInterpreter::vtkHesaiPacketInterpreter()
  : Internals(new vtkHesaiPacketInterpreter::vtkInternals())
{
  this->Internals->CreateDecoder(
    std::bind(&vtkHesaiPacketInterpreter::SplitFrame, this, std::placeholders::_1));
  this->ResetCurrentFrame();
  this->SetSensorVendor("Hesai");
  this->SetSensorModelName(this->Internals->GetLidarModelName(this->LidarModel));
}

//-----------------------------------------------------------------------------
vtkHesaiPacketInterpreter::~vtkHesaiPacketInterpreter() = default;

//-----------------------------------------------------------------------------
void vtkHesaiPacketInterpreter::ProcessPacket(unsigned char const* data, unsigned int dataLength)
{
  if (!this->IsLidarPacket(data, dataLength))
  {
    return;
  }
  auto& internals = this->Internals;
  internals->IsPreProcessing = false;

  if (this->IsPandar128())
  {
    internals->SwiftDecoder->ProcessLidarPacket(data, dataLength);
  }
  else
  {
    const uint8_t* pkt = reinterpret_cast<const uint8_t*>(data);
    internals->GeneralDecoder->ProcessLidarPacket(pkt, dataLength, 0.0);
  }
}

//-----------------------------------------------------------------------------
bool vtkHesaiPacketInterpreter::IsLidarPacket(unsigned char const* data, unsigned int dataLength)
{
  auto& internals = this->Internals;
  return this->IsPandar128() ? internals->SwiftDecoder->IsValidPacket(data, dataLength)
                             : internals->GeneralDecoder->IsValidPacket(dataLength);
}

//-----------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> vtkHesaiPacketInterpreter::CreateNewEmptyFrame(vtkIdType nbrOfPoints,
  vtkIdType prereservedNbrOfPoints)
{
  auto& internals = this->Internals;
  const vtkIdType defaultPrereservedNbrOfPoints = this->IsPandar128()
    ? internals->SwiftDecoder->GetMaxPointCloudNum()
    : internals->GeneralDecoder->GetMaxPointCloudNum();
  prereservedNbrOfPoints =
    std::max(static_cast<vtkIdType>(prereservedNbrOfPoints * 1.5), defaultPrereservedNbrOfPoints);
  vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();

  vtkNew<vtkPoints> points;
  points->SetDataTypeToFloat();
  points->Allocate(prereservedNbrOfPoints);
  if (nbrOfPoints > 0)
  {
    points->SetNumberOfPoints(nbrOfPoints);
  }
  points->GetData()->SetName("Points");
  polyData->SetPoints(points.GetPointer());

  internals->Points = points.GetPointer();
  // clang-format off
  InitArrayForPolyData(true, internals->PointsX, "X", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, internals->PointsY, "Y", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(true, internals->PointsZ, "Z", nbrOfPoints, prereservedNbrOfPoints, polyData, this->EnableAdvancedArrays);
  InitArrayForPolyData(false, internals->Intensity, "intensity", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->LaserId, "laser_id", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Timestamp, "timestamp", nbrOfPoints, prereservedNbrOfPoints, polyData);
  InitArrayForPolyData(false, internals->Distance, "distance_m", nbrOfPoints, prereservedNbrOfPoints, polyData);
  // clang-format on

  // Set the default array to display in the application
  polyData->GetPointData()->SetActiveScalars("intensity");
  return polyData;
}

//-----------------------------------------------------------------------------
bool vtkHesaiPacketInterpreter::PreProcessPacket(unsigned char const* data,
  unsigned int dataLength,
  double& outLidarDataTime)
{
  auto& internals = this->Internals;
  internals->IsPreProcessing = true;

  bool splitPacket = false;
  if (this->IsPandar128())
  {
    auto& decoder = this->Internals->SwiftDecoder;
    splitPacket = decoder->PreProcessLidarPacket(data, dataLength);
    outLidarDataTime = decoder->GetLastTimestamp();
  }
  else
  {
    assert(this->PreferredReturnMode < 3);
    auto& decoder = this->Internals->GeneralDecoder;
    decoder->SetSkipPointsInMultipleReturns(this->SkipPointMultipleReturn,
      static_cast<hs_general::HS_DualReturnModes>(this->PreferredReturnMode));

    const uint8_t* pkt = reinterpret_cast<const uint8_t*>(data);
    splitPacket = decoder->ProcessLidarPacket(pkt, dataLength, outLidarDataTime);
    outLidarDataTime = decoder->GetLastTimestamp();
  }
  return splitPacket;
}

//-----------------------------------------------------------------------------
void vtkHesaiPacketInterpreter::Initialize()
{
  std::string filename;
  if (this->CalibrationFileName != nullptr)
  {
    filename = this->CalibrationFileName;
  }

  bool hasCalibrationFile = !filename.empty();
  if (hasCalibrationFile)
  {
    const auto path = std::filesystem::path(filename);
    if (path.extension() != ".csv")
    {
      return;
    }

    if (this->IsPandar128())
    {
      auto& decoder = this->Internals->SwiftDecoder;
      decoder->ResetCalibration();
      decoder->LoadCorrectionFile(filename);
    }
    else
    {
      this->Internals->GeneralDecoder->LoadCorrectionFile(filename);
    }
  }
  else
  {
    if (this->IsPandar128())
    {
      this->Internals->SwiftDecoder->ResetCalibration();
    }
    else
    {
      auto& decoder = this->Internals->GeneralDecoder;
      const std::vector<std::string> supportedList = decoder->GetSupportedLidarList();
      if (!decoder->SetLidarType(supportedList[this->LidarModel]))
      {
        vtkErrorMacro("Unknown Hesai interpreter type.");
        return;
      }
    }
  }
  Superclass::Initialize();
}

//-----------------------------------------------------------------------------
bool vtkHesaiPacketInterpreter::SplitFrame(double vtkNotUsed(timestamp))
{
  auto& internals = this->Internals;
  const uint32_t numberOfPktPoints = this->IsPandar128()
    ? internals->SwiftDecoder->GetNumberOfPacketPoints()
    : internals->GeneralDecoder->GetNumberOfPacketPoints();

  if (internals->Points->GetNumberOfPoints() <= numberOfPktPoints)
  {
    return false;
  }
  return this->Superclass::SplitFrame();
}

//-----------------------------------------------------------------------------
bool vtkHesaiPacketInterpreter::IsPandar128()
{
  return this->LidarModel == 128;
}

//-----------------------------------------------------------------------------
void vtkHesaiPacketInterpreter::SetLidarModel(int model)
{
  if (this->LidarModel == model)
  {
    return;
  }

  const size_t listSize = this->Internals->GeneralDecoder->GetSupportedLidarList().size();

  bool isPandar128 = model == 128;
  if (isPandar128 || (0 <= model && static_cast<size_t>(model) < listSize))
  {
    this->LidarModel = model;
    this->SetSensorModelName(this->Internals->GetLidarModelName(model));
    this->Modified();
    this->ResetInitializedState();
  }
}
