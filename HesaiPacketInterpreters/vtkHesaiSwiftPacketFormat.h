#ifndef vtkHesaiSwiftPacketFormat_H
#define vtkHesaiSwiftPacketFormat_H

#include <boost/endian/arithmetic.hpp>
#include <memory>

//----------------------------------------------------------------------------
//! @brief Simple getter that handles conversion to native unsigned integer types.
#define GET_NATIVE_UINT(n, attr)                                                                   \
  uint##n##_t Get##attr() const                                                                    \
  {                                                                                                \
    return this->attr;                                                                             \
  }
#define SET_NATIVE_UINT(n, attr)                                                                   \
  void Set##attr(uint##n##_t x)                                                                    \
  {                                                                                                \
    this->attr = x;                                                                                \
  }

#define BIT(n) (1 << (n))
//! Create a bitmask of length \a len.
#define BIT_MASK(len) (BIT(len) - 1)

//! Create a bitfield mask of length \a starting at bit \a start.
#define BF_MASK(start, len) (BIT_MASK(len) << (start))

//! Extract a bitfield of length \a len starting at bit \a start from \a y.
#define BF_GET(y, start, len) (((y) >> (static_cast<decltype(y)>(start))) & BIT_MASK(len))

//! Prepare a bitmask for insertion or combining.
#define BF_PREP(x, start, len) (((x)&BIT_MASK(len)) << (start))

//! Insert a new bitfield value x into y.
#define BF_SET(y, x, start, len) (y = ((y) & ~BF_MASK(start, len)) | BF_PREP(x, start, len))

#define PANDAR128_POINT_NUM (460800)
#define PANDAR128_LASER_NUM (128)
#define PANDAR128_BLOCK_NUM (2)
#define MAX_BLOCK_NUM (8)
#define PANDAR128_DISTANCE_UNIT (0.004)
#define PANDAR128_SOB_SIZE (2)
#define PANDAR128_VERSION_MAJOR_SIZE (1)
#define PANDAR128_VERSION_MINOR_SIZE (1)
#define PANDAR128_HEAD_RESERVED1_SIZE (2)
#define PANDAR128_LASER_NUM_SIZE (1)
#define PANDAR128_BLOCK_NUM_SIZE (1)
#define PANDAR128_ECHO_COUNT_SIZE (1)
#define PANDAR128_ECHO_NUM_SIZE (1)
#define PANDAR128_HEAD_RESERVED2_SIZE (2)
#define PANDAR128_HEAD_SIZE                                                                        \
  (PANDAR128_SOB_SIZE + PANDAR128_VERSION_MAJOR_SIZE + PANDAR128_VERSION_MINOR_SIZE +              \
    PANDAR128_HEAD_RESERVED1_SIZE + PANDAR128_LASER_NUM_SIZE + PANDAR128_BLOCK_NUM_SIZE +          \
    PANDAR128_ECHO_COUNT_SIZE + PANDAR128_ECHO_NUM_SIZE + PANDAR128_HEAD_RESERVED2_SIZE)
#define PANDAR128_AZIMUTH_SIZE (2)
#define DISTANCE_SIZE (2)
#define INTENSITY_SIZE (1)
#define CONFIDENCE_SIZE (1)
#define PANDAR128_UNIT_SIZE (DISTANCE_SIZE + INTENSITY_SIZE)
#define PANDAR128_BLOCK_SIZE (PANDAR128_UNIT_SIZE * PANDAR128_LASER_NUM + PANDAR128_AZIMUTH_SIZE)
#define PANDAR128_TAIL_RESERVED1_SIZE (3)
#define PANDAR128_TAIL_RESERVED2_SIZE (3)
#define PANDAR128_SHUTDOWN_FLAG_SIZE (1)
#define PANDAR128_TAIL_RESERVED3_SIZE (3)
#define PANDAR128_MOTOR_SPEED_SIZE (2)
#define PANDAR128_TS_SIZE (4)
#define PANDAR128_RETURN_MODE_SIZE (1)
#define PANDAR128_FACTORY_INFO (1)
#define PANDAR128_UTC_SIZE (6)
#define PANDAR128_TAIL_SIZE                                                                        \
  (PANDAR128_TAIL_RESERVED1_SIZE + PANDAR128_TAIL_RESERVED2_SIZE + PANDAR128_SHUTDOWN_FLAG_SIZE +  \
    PANDAR128_TAIL_RESERVED3_SIZE + PANDAR128_MOTOR_SPEED_SIZE + PANDAR128_TS_SIZE +               \
    PANDAR128_RETURN_MODE_SIZE + PANDAR128_FACTORY_INFO + PANDAR128_UTC_SIZE)
#define PANDAR128_PACKET_SIZE                                                                      \
  (PANDAR128_HEAD_SIZE + PANDAR128_BLOCK_SIZE * PANDAR128_BLOCK_NUM + PANDAR128_TAIL_SIZE)
#define PANDAR128_SEQ_NUM_SIZE (4)
#define PANDAR128_PACKET_SEQ_NUM_SIZE (PANDAR128_PACKET_SIZE + PANDAR128_SEQ_NUM_SIZE)
#define PANDAR128_WITHOUT_CONF_UNIT_SIZE (DISTANCE_SIZE + INTENSITY_SIZE)

#define TASKFLOW_STEP_SIZE (225)
#define PANDAR128_CRC_SIZE (4)
#define PANDAR128_FUNCTION_SAFETY_SIZE (17)

#define PANDAR128_CIRCLE (36000)

#pragma pack(push, 1)
//! @brief class representing the raw measure
/*
   0               1               2               3
   0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |           Distance         |            Intensity             |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
class Pandar128Unit
{
private:
  // Distance
  boost::endian::little_uint16_t Distance;

  // Intensity
  boost::endian::little_uint8_t Intensity;

public:
  GET_NATIVE_UINT(16, Distance)
  SET_NATIVE_UINT(16, Distance)
  GET_NATIVE_UINT(8, Intensity)
  SET_NATIVE_UINT(8, Intensity)
};
#pragma pack(pop)

#pragma pack(push, 1)
//! @brief class representing the raw block
/*
   0               1               2
   0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
  |             Azimuth            |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

 */
class Pandar128Block
{
private:
  // Azimuth
  boost::endian::little_uint16_t Azimuth;

public:
  Pandar128Unit units[PANDAR128_LASER_NUM];

public:
  GET_NATIVE_UINT(16, Azimuth)
  SET_NATIVE_UINT(16, Azimuth)
};
#pragma pack(pop)

#pragma pack(push, 1)
//! @brief class representing the Raw packet
/*
   0               1               2               3
   0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |               Sob              |  VersionMajor | VersionMinor |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |    DistUnit    |     Flags     |   LaserNum    |  BlockNum    |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |    EchoCount   |   EchoNum     |          Reserved            |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
class Pandar128Header
{
private:
  boost::endian::little_uint16_t Sob;

  boost::endian::little_uint8_t VersionMajor;

  boost::endian::little_uint8_t VersionMinor;

  boost::endian::little_uint8_t DistUnit;

  boost::endian::little_uint8_t Flags;

  boost::endian::little_uint8_t LaserNum;

  boost::endian::little_uint8_t BlockNum;

  boost::endian::little_uint8_t EchoCount;

  boost::endian::little_uint8_t EchoNum;

  boost::endian::little_uint16_t Reserved;

public:
  GET_NATIVE_UINT(16, Sob)
  SET_NATIVE_UINT(16, Sob)
  GET_NATIVE_UINT(8, VersionMajor)
  SET_NATIVE_UINT(8, VersionMajor)
  GET_NATIVE_UINT(8, VersionMinor)
  SET_NATIVE_UINT(8, VersionMinor)
  GET_NATIVE_UINT(8, DistUnit)
  SET_NATIVE_UINT(8, DistUnit)
  GET_NATIVE_UINT(8, Flags)
  SET_NATIVE_UINT(8, Flags)
  GET_NATIVE_UINT(8, LaserNum)
  SET_NATIVE_UINT(8, LaserNum)
  GET_NATIVE_UINT(8, BlockNum)
  SET_NATIVE_UINT(8, BlockNum)
  GET_NATIVE_UINT(8, EchoCount)
  SET_NATIVE_UINT(8, EchoCount)
  GET_NATIVE_UINT(8, EchoNum)
  SET_NATIVE_UINT(8, EchoNum)
  GET_NATIVE_UINT(16, Reserved)
  SET_NATIVE_UINT(16, Reserved)
};
#pragma pack(pop)

// https://github.com/HesaiTechnology/HesaiLidar_Pandar128_ROS/blob/160436018cbe4c96249c281499e2e9638d38b39c/pandar_pointcloud/src/conversions/convert.cc
// https://github.com/HesaiTechnology/HesaiLidar_Pandar128_ROS/blob/160436018cbe4c96249c281499e2e9638d38b39c/pandar_pointcloud/src/conversions/convert.h

#pragma pack(push, 1)
//! @brief class representing the Raw packet
/*
   0               1               2               3
   0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |    Reserved1   |   Reserved1   |  Reserved1    |  Reserved2   |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |    Reserved2   |   Reserved2   | ShutdownFlag  |  Reserved3   |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |    Reserved3   |   Reserved3   |         MotorSpeed           |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                            Timestamp                          |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |   ReturnMode   | FactoryInfo   |   UTCTime     |   UTCTime    |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |     UTCTime    |    UTCTime    |   UTCTime     |  UTCTime     |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                             SeqNum                            |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
class Pandar128Tail
{
private:
  boost::endian::little_uint8_t Reserved11;
  boost::endian::little_uint8_t Reserved12;
  boost::endian::little_uint8_t Reserved13;

  boost::endian::little_uint8_t Reserved21;
  boost::endian::little_uint8_t Reserved22;
  boost::endian::little_uint8_t Reserved23;

  boost::endian::little_uint16_t Padding1; //+
  boost::endian::little_uint16_t Padding2; //+
  boost::endian::little_uint8_t Padding3;  //+

  boost::endian::little_uint8_t ShutdownFlag;

  // boost::endian::little_uint8_t Reserved3[3];

  boost::endian::little_uint8_t ReturnMode;

  boost::endian::little_uint16_t MotorSpeed;

  boost::endian::little_uint8_t UTCTime0;
  boost::endian::little_uint8_t UTCTime1;
  boost::endian::little_uint8_t UTCTime2;
  boost::endian::little_uint8_t UTCTime3;
  boost::endian::little_uint8_t UTCTime4;
  boost::endian::little_uint8_t UTCTime5;

  boost::endian::little_uint32_t Timestamp;

  // boost::endian::little_uint8_t FactoryInfo;

  boost::endian::little_uint32_t SeqNum;

public:
  GET_NATIVE_UINT(8, ShutdownFlag)
  SET_NATIVE_UINT(8, ShutdownFlag)
  GET_NATIVE_UINT(16, MotorSpeed)
  SET_NATIVE_UINT(16, MotorSpeed)
  GET_NATIVE_UINT(32, Timestamp)
  SET_NATIVE_UINT(32, Timestamp)
  GET_NATIVE_UINT(8, ReturnMode)
  SET_NATIVE_UINT(8, ReturnMode)
  GET_NATIVE_UINT(32, SeqNum)
  SET_NATIVE_UINT(32, SeqNum)

  GET_NATIVE_UINT(8, UTCTime0)
  SET_NATIVE_UINT(8, UTCTime0)
  GET_NATIVE_UINT(8, UTCTime1)
  SET_NATIVE_UINT(8, UTCTime1)
  GET_NATIVE_UINT(8, UTCTime2)
  SET_NATIVE_UINT(8, UTCTime2)
  GET_NATIVE_UINT(8, UTCTime3)
  SET_NATIVE_UINT(8, UTCTime3)
  GET_NATIVE_UINT(8, UTCTime4)
  SET_NATIVE_UINT(8, UTCTime4)
  GET_NATIVE_UINT(8, UTCTime5)
  SET_NATIVE_UINT(8, UTCTime5)
};
#pragma pack(pop)

#pragma pack(push, 1)
//! @brief class representing the Hesai Packet
struct Pandar128Packet
{
  Pandar128Header header;

  Pandar128Block blocks[PANDAR128_BLOCK_NUM];

  // PANDAR128_CRC_SIZE is equal to 4
  boost::endian::little_uint32_t crc;

  // PANDAR128_FUNCTION_SAFETY_SIZE is equal to 17
  boost::endian::little_uint8_t functionSafety[17];

  Pandar128Tail tail;
};
#pragma pack(pop)

// clang-format off
static const float elev_angle[] = {
    14.436f,  13.535f,  13.08f,   12.624f,  12.163f,  11.702f,  11.237f,
    10.771f,  10.301f,  9.83f,    9.355f,   8.88f,    8.401f,   7.921f,
    7.437f,   6.954f,   6.467f,   5.98f,    5.487f,   4.997f,   4.501f,
    4.009f,   3.509f,   3.014f,   2.512f,   2.014f,   1.885f,   1.761f,
    1.637f,   1.511f,   1.386f,   1.258f,   1.13f,    1.009f,   0.88f,
    0.756f,   0.63f,    0.505f,   0.379f,   0.251f,   0.124f,   0.0f,
    -0.129f,  -0.254f,  -0.38f,   -0.506f,  -0.632f,  -0.76f,   -0.887f,
    -1.012f,  -1.141f,  -1.266f,  -1.393f,  -1.519f,  -1.646f,  -1.773f,
    -1.901f,  -2.027f,  -2.155f,  -2.282f,  -2.409f,  -2.535f,  -2.662f,
    -2.789f,  -2.916f,  -3.044f,  -3.172f,  -3.299f,  -3.425f,  -3.552f,
    -3.680f,  -3.806f,  -3.933f,  -4.062f,  -4.190f,  -4.318f,  -4.444f,
    -4.571f,  -4.698f,  -4.824f,  -4.951f,  -5.081f,  -5.209f,  -5.336f,
    -5.463f,  -5.589f,  -5.717f,  -5.843f,  -5.968f,  -6.099f,  -6.607f,
    -7.118f,  -7.624f,  -8.135f,  -8.64f,   -9.149f,  -9.652f,  -10.16f,
    -10.664f, -11.17f,  -11.67f,  -12.174f, -12.672f, -13.173f, -13.668f,
    -14.166f, -14.658f, -15.154f, -15.643f, -16.135f, -16.62f,  -17.108f,
    -17.59f,  -18.073f, -18.548f, -19.031f, -19.501f, -19.981f, -20.445f,
    -20.92f,  -21.379f, -21.85f,  -22.304f, -22.77f,  -23.219f, -23.68f,
    -24.123f, -25.016f,
};

static const float azimuth_offset[] = {
    3.257f,  3.263f, -1.083f, 3.268f, -1.086f, 3.273f,  -1.089f, 3.278f,
    -1.092f, 3.283f, -1.094f, 3.288f, -1.097f, 3.291f,  -1.1f,   1.1f,
    -1.102f, 1.1f,   -3.306f, 1.102f, -3.311f, 1.103f,  -3.318f, 1.105f,
    -3.324f, 1.106f, 7.72f,   5.535f, 3.325f,  -3.33f,  -1.114f, -5.538f,
    -7.726f, 1.108f, 7.731f,  5.543f, 3.329f,  -3.336f, -1.116f, -5.547f,
    -7.738f, 1.108f, 7.743f,  5.551f, 3.335f,  -3.342f, -1.119f, -5.555f,
    -7.75f,  1.11f,  7.757f,  5.56f,  3.34f,   -3.347f, -1.121f, -5.564f,
    -7.762f, 1.111f, 7.768f,  5.569f, 3.345f,  -3.353f, -1.123f, -5.573f,
    -7.775f, 1.113f, 7.780f,  5.578f, 3.351f,  -3.358f, -1.125f, -5.582f,
    -7.787f, 1.115f, 7.792f,  5.586f, 3.356f,  -3.363f, -1.126f, -5.591f,
    -7.799f, 1.117f, 7.804f,  5.595f, 3.36f,   -3.369f, -1.128f, -5.599f,
    -7.811f, 1.119f, -3.374f, 1.12f,  -3.379f, 1.122f,  -3.383f, 3.381f,
    -3.388f, 3.386f, -1.135f, 3.39f,  -1.137f, 3.395f,  -1.138f, 3.401f,
    -1.139f, 3.406f, -1.14f,  3.41f,  -1.141f, 3.416f,  -1.142f, 1.14f,
    -1.143f, 1.143f, -3.426f, 1.146f, -3.429f, 1.147f,  -3.433f, 1.15f,
    -3.436f, 1.152f, -3.44f,  1.154f, -3.443f, 1.157f,  -3.446f, -3.449f,
};
// clang-format on

#endif // vtkHesaiSwiftPacketFormat_H
