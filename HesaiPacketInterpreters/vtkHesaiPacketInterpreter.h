/*=========================================================================

  Program:   LidarView
  Module:    vtkHesaiPacketInterpreter.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkHesaiPacketInterpreter_h
#define vtkHesaiPacketInterpreter_h

#include <vtkLidarPacketInterpreter.h>

#include <memory>

#include "HesaiPacketInterpretersModule.h" // for export macro

/**
 * vtkHesaiPacketInterpreter is currently handling the following hesai
 * lidar models:
 *   "Pandar40P", "Pandar40M", "Pandar64", "Pandar20A", "Pandar20B",
 *   "PandarQT", "PandarXT-32", "PandarXT-16", "PandarXTM" and "Pandar128".
 */
class HESAIPACKETINTERPRETERS_EXPORT vtkHesaiPacketInterpreter : public vtkLidarPacketInterpreter
{
public:
  static vtkHesaiPacketInterpreter* New();
  vtkTypeMacro(vtkHesaiPacketInterpreter, vtkLidarPacketInterpreter)

  void Initialize() override;

  bool PreProcessPacket(unsigned char const* data,
    unsigned int dataLength,
    double& outLidarDataTime) override;

  bool IsLidarPacket(unsigned char const* data, unsigned int dataLength) override;

  void ProcessPacket(unsigned char const* data, unsigned int dataLength) override;

  vtkSetMacro(SkipPointMultipleReturn, bool);
  vtkGetMacro(SkipPointMultipleReturn, bool);

  vtkSetMacro(PreferredReturnMode, bool);
  vtkGetMacro(PreferredReturnMode, bool);

  void SetLidarModel(int type);
  vtkGetMacro(LidarModel, int);

protected:
  vtkSmartPointer<vtkPolyData> CreateNewEmptyFrame(vtkIdType numberOfPoints,
    vtkIdType prereservedNumberOfPoints = 60000) override;

  int LidarModel = 0;
  bool SkipPointMultipleReturn = false;
  int PreferredReturnMode = 0;

  vtkHesaiPacketInterpreter();
  ~vtkHesaiPacketInterpreter();

private:
  vtkHesaiPacketInterpreter(const vtkHesaiPacketInterpreter&) = delete;
  void operator=(const vtkHesaiPacketInterpreter&) = delete;

  bool SplitFrame(double timestamp);
  bool IsPandar128();

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif // vtkHesaiPacketInterpreter_h
