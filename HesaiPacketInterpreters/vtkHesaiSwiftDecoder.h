/*=========================================================================

  Program:   LidarView
  Module:    vtkHesaiSwiftDecoder.h

  Copyright (c) Kitware Inc.
  All rights reserved.
  See LICENSE or http://www.apache.org/licenses/LICENSE-2.0 for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkHesaiSwiftDecoder_h
#define vtkHesaiSwiftDecoder_h

#include <functional>
#include <memory>
#include <iostream>
#include "HesaiPacketInterpretersModule.h" // for export macro

class HESAIPACKETINTERPRETERS_EXPORT vtkHesaiSwiftDecoder
{
public:
  struct Point
  {
    double x;
    double y;
    double z;
    float intensity;
    double timestamp;
    uint16_t ring;
  };

  vtkHesaiSwiftDecoder(std::function<void(Point point)> fillPointsCallback,
    std::function<void(double timestamp)> splitFrameCallback);
  ~vtkHesaiSwiftDecoder();

  void ResetCalibration();
  void LoadCorrectionFile(std::string filename);

  bool IsValidPacket(unsigned char const* data, unsigned int dataLength);

  void ProcessLidarPacket(unsigned char const* data, unsigned int dataLength);
  bool PreProcessLidarPacket(unsigned char const* data, unsigned int dataLength);

  double GetLastTimestamp();

  uint32_t GetMaxPointCloudNum();
  uint32_t GetNumberOfPacketPoints();

private:
  vtkHesaiSwiftDecoder(const vtkHesaiSwiftDecoder&) = delete;
  void operator=(const vtkHesaiSwiftDecoder&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;

  std::function<void(Point point)> FillPointsCallback;
  std::function<void(double timestamp)> SplitFrameCallback;
};

#endif
