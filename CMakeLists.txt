set(calib_files
  Pandar128.csv
  Pandar40M.csv
  Pandar40P.csv
  Pandar64.csv
  PandarQT.csv
  PandarXT16.csv
  PandarXT32.csv
  PandarXT32-old.csv
  PandarXTM.csv
)

paraview_add_plugin(HesaiPlugin
  REQUIRED_ON_CLIENT
  REQUIRED_ON_SERVER
  VERSION "2.0"
  MODULES
    HesaiPlugin::HesaiGeneralSDK
    HesaiPlugin::HesaiPacketInterpreters
    HesaiPlugin::HesaiSwiftSDK
  MODULE_FILES
    "${CMAKE_CURRENT_SOURCE_DIR}/HesaiGeneralSDK/vtk.module"
    "${CMAKE_CURRENT_SOURCE_DIR}/HesaiPacketInterpreters/vtk.module"
    "${CMAKE_CURRENT_SOURCE_DIR}/HesaiSwiftSDK/sources/vtk.module"
  SERVER_MANAGER_XML
    "${CMAKE_CURRENT_SOURCE_DIR}/HesaiProxies.xml"
)

# Copy the calibration to the build folder
foreach(file ${calib_files})
  configure_file("CalibrationFiles/${file}" "${PROJECT_BINARY_DIR}/share/${file}" COPYONLY)
endforeach()

# Install step
foreach(file ${calib_files})
  install(
    FILES "${PROJECT_BINARY_DIR}/share/${file}"
    DESTINATION "share"
  )
endforeach()
