# Hesai Plugin

This plugin wrap multiple Hesai drivers in a ParaView plugin. This allow LidarView to be able to some Hesai LiDARs.

Driver wrapped:
- [Hesai General SDK](https://github.com/HesaiTechnology/HesaiLidar_General_SDK): add support for `Pandar40P`, `Pandar40M`, `Pandar64`, `Pandar20A`, `Pandar20B` `PandarQT`, `PandarXT-32`, `PandarXT-16` and `PandarXTM`.
- [Hesai Swift SDK](https://github.com/HesaiTechnology/HesaiLidar_Swift_ROS): add experimental support for `Pandar128`

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
