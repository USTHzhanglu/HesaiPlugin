# HesaiLidar_General_SDK fork for LidarView

This branch contains changes required to embed hesai general driver into LidarView
through a VTK module. This includes changes made primarily to the build system
to allow it to be embedded into another source tree.

  * Integrate CMake code with VTK's module system
  * Use a simplified structure instead of pcl one.
  * Remove Hesai object detection algorithms (requires boost)
  * Remove Hesai reading pcap / streaming mechanism (require boost)
  * Do not use environment variables
  * Adapt PandarGeneral_internal API to LidarView needs:
    - Simplify constructor
    - Externalize ProcessLidarPacket() main loop and make the method public
    - Call a callback to fill points
    - Include missing std libraries, remove pcl and boost dependencies
  * Add SetLidarType and GetSupportedLidarList methods
  * Wrap used file into a hesai namespace
  * Remove unecessary std::cout calls
  * Add a SetSkipPointsInMultipleReturns method that allow user to remove one of
    the dual return when activated. (By default either FIRST or STRONGEST)
    Note that block indices starts to 1 in docs, so odd numbers become even
    and vice-versa.
